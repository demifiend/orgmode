#+TITLE: How to Contact Me
#+DESCRIPTION: If you need to get in touch with me, here's my contact info
#+KEYWORDS: contact, email, diaspora, mastodon, google+, facebook, twitter
#+DATE: <2018-10-24 Wed>
#+SETUPFILE: ~/org/personal/assets/options.orgmode

The best way to reach me is by email.

- Send friendly messages to *mgraybosch at fastmail dot com*.
- Send confidential messages to *matthewgraybosch at tutanota dot com*.

I'm serious. I actually miss getting email from human beings, so if
you want to drop me a line you're welcome to do so.

* Federated Social Media

Want to use social media? Get an account on a federated service like
Diaspora, Pleroma, or Mastodon.

- [[https://octodon.social/@starbreaker][@starbreaker@octodon.social]]
- [[https://pleroma.site/users/starbreaker][@starbreaker@pleroma.site]]
- [[https://bsd.network/@starbreaker][@starbreaker@bsd.network]]
- [[https://pluspora.com/people/75778ca0ad5601364692005056268def][@starbreaker@pluspora.com]]

* Corporate Social Media

While I have social media accounts, I maintain them mainly because
having them and not using them is less of a pain in the ass than not
having them.

- Google+: [[https://plus.google.com/+MatthewGraybosch][Matthew Graybosch]]
- Twitter: [[https://twitter.com/MJGraybosch][@MJGraybosch]]
- Facebook: [[https://facebook.com/matthew.graybosch10][Matthew Graybosch]]

* Instant Messages

If you prefer instant messaging, you can reach me via [[https://wire.com][Wire]]. I'm
@matthewgraybosch on that service.

* GitLab

I have a [[https://gitlab.com/mgraybosch][GitLab profile]] since I use the service to host git
repositories for my websites and my [[https://starbreakersaga.com][Starbreaker]] project.
