#+TITLE: What's Happening Now
#+DESCRIPTION: Here's a general overview of what's going on in my life.
#+KEYWORDS: now, status
#+DATE: <2018-10-24 Wed>
#+SETUPFILE: ~/org/personal/assets/options.orgmode

/(This is a [[https://nownownow.com/about][now page]], and if you have your own site, [[https://nownownow.com/about][you should make one]], too.)/

I'm in Harrisburg, PA. It's autumn. Mid-term elections are
imminent. I'm watching our leaders fiddle while the world threatens to
burn.

* Writing

The last [[https://starbreakersaga.com][Starbreaker]] story I finished was [[https://starbreakersaga.com/stories/thirteen-cuts.html]["Thirteen Cuts"]], a 5,000
word short that I wrote in August and published in September.

I'm not at all happy that I haven't managed to make any progress with
the novel I wanted to write this year, /Dissident Aggressor/. It was bad
enough that I had to put /Shattered Guardian/ on hold because it wasn't
time to write that one yet.

In the meantime, I'll be attending the 2018 World Fantasy Convention.

* Day Job

I currently work for Deloitte Consulting as a "solution specialist" --
their term for a mid-level software developer. I don't speak for my
employers or represent them, nor am I at liberty to discuss the work I
do for them.

* Unix

I've been running [[https://openbsd.org][OpenBSD]] for a year now, after almost 20 years of
distro-hopping. It does everything I need, the performance is /good
enough/, and if one of my machines gets pwned it'll be my own damn
fault.

* Internet/Social Media

With [[https://plus.google.com/+MatthewGraybosch][Google+]] shutting down, I've had an opportunity to take stock of
my involvement in social media. While I've joined other G+ refugees on
a certain [[https://pluspora.com/people/75778ca0ad5601364692005056268def][Diaspora* node]], I've decided that I don't /want/ to spend as
much time on the internet as I used to.

I'm still lonely after chatting with people on social media, so it's
not worth it any longer.

I'm not going to go away, though. When I write a blog post or
serialize a story, I'll mirror the text on Diaspora* and have the
original link to the Diaspora* post so that anybody who wants to
comment (and has a Diaspora* account) can do so.

But I'd rather get email from real people. 99.999% of the email I get
nowadays is from machines.

* Personal

I've come to the realization that while I have a male body, I don't
see myself as a man. I don't see myself as a woman, either. I suppose
that means I'm nonbinary, rather than transgender.

I still like women, and love my wife, so you can make of this whatever
you like as long as you don't burden me with ignorant opinions.

/This now page is current as of 24 October 2018./
